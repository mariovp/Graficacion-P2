package proyecto;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by Mario on 18/04/2016.
 */
public class Main{


    private JPanel MainPanel;
    private Dibujo dibujo1;
    private JTextField textSx;
    private JTextField textSy;
    private JTextField textTx;
    private JTextField textTy;
    private JButton btnTrasladar;
    private JButton btnEscalar;
    private JCheckBox checkPFesc;
    private JTextField textXfEsc;
    private JTextField textYfEsc;
    private JTextField textTheta;
    private JButton btnRotar;
    private JCheckBox checkPFrot;
    private JTextField textXrRot;
    private JTextField textYrRot;
    private JCheckBox reflexiónCheckBox;
    private JRadioButton respectoAXRadioButton;
    private JRadioButton respectoAYRadioButton;
    private JButton btnCompuesta;

    public Main() {
        btnTrasladar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dibujo1.trasladar(Integer.parseInt(textTx.getText().trim()),Integer.parseInt(textTy.getText().trim()));
            }
        });
        checkPFesc.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange()==ItemEvent.SELECTED){
                    textXfEsc.setEnabled(true);
                    textYfEsc.setEnabled(true);
                }else{
                    textXfEsc.setEnabled(false);
                    textYfEsc.setEnabled(false);
                }
            }
        });
        btnEscalar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int Xf=0;
                int Yf=0;

                try {
                    Xf=Integer.parseInt(textXfEsc.getText().trim());
                    Yf=Integer.parseInt(textYfEsc.getText().trim());
                } catch (NumberFormatException e1) {
                    Xf=-1;
                    Yf=-1;
                }

                dibujo1.escalar(Double.parseDouble(textSx.getText().trim()),Double.parseDouble(textSy.getText().trim()),Xf,Yf);
            }
        });
        checkPFrot.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange()==ItemEvent.SELECTED){
                    textXrRot.setEnabled(true);
                    textYrRot.setEnabled(true);
                }else{
                    textXrRot.setEnabled(false);
                    textYrRot.setEnabled(false);
                }
            }
        });
        btnRotar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Xf=0;
                int Yf=0;
                double rad;
                try {
                    Xf=Integer.parseInt(textXrRot.getText().trim());
                    Yf=Integer.parseInt(textYrRot.getText().trim());
                } catch (NumberFormatException e1) {
                    Xf=-1;
                    Yf=-1;
                }

                rad=(Double.parseDouble(textTheta.getText().trim())*Math.PI)/180;

                dibujo1.rotar(rad,Xf,Yf);
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Proyecto Tranformaciones 2D");
        frame.setContentPane(new Main().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
        dibujo1 = new Dibujo();
        dibujo1.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    }
}
