/*Mario Valenzuela Partida
 * Graficación 13-14*/
package proyecto;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.Arrays;

public class Dibujo extends JPanel {

    private BufferedImage buffImage;
    private WritableRaster raster;
    private int[] color;
    private int[][] figO;
    private int[][] figT;

    public Dibujo() {
        color = new int[]{0,0,0,255};
        buffImage = new BufferedImage(500,500,BufferedImage.TYPE_INT_ARGB);
        raster = buffImage.getRaster();
        figO = new int[][]{{200, 200, 300, 200},{200, 300, 300, 300},{200, 200, 200, 300},{300, 200, 300, 300}};
        figT = new int[4][4];
    }

    //Aquí se crea el dibujo original
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        dibujaCuadrado(figO);

        g.drawImage(buffImage, 0, 0, this);
    }

    private void dibujaCuadrado(int[][] fig) {
        int x1=0, y1=1, x2=2, y2=3;


        //Horizontales
        //Arriba
        color= new int[]{255,0,0,255};
        bres_line(fig[0][x1], fig[0][y1], fig[0][x2], fig[0][y2]);
        //Abajo
        color= new int[]{0,255,0,255};
        bres_line(fig[1][x1], fig[1][y1], fig[1][x2], fig[1][y2]);

        //Verticales
        //Izquierda
        color= new int[]{0,0,255,255};
        bres_line(fig[2][x1], fig[2][y1], fig[2][x2], fig[2][y2]);
        //Derecha
        color= new int[]{220,220,0,255};
        bres_line(fig[3][x1], fig[3][y1], fig[3][x2], fig[3][y2]);

        color= new int[]{200,200,200,255};
        boundary_fill(250,250,color);

    }

    //Bresenham modificado para todas las pendientes
    public void bres_line(int x1,int y1,int x2,int y2 ){
        int x, y, dx, dy, p, const1, const2, stepX, stepY;

        //Delta (diferencia) de X
        dx = (x2 - x1);
        //Delta (diferencia) de Y
        dy = (y2 - y1);

        //Checar dirección en X
        if (dx < 0) {
            //Entonces dx es negativo (dibuja hacia la izquierda)
            dx = -dx;
            stepX = -1;
        }
        else {
            //Si no, continua positivo
            stepX = 1;
        }

        //Checar dirección en Y
        if (dy < 0) {
            //Entonces dy es negativo
            dy = -dy;
            stepY = -1;
        } else{
            //Si no, continua positivo
            stepY = 1;
        }

        //Se dibuja el primer pixel de la línea
        x = x1;
        y = y1;
        setPixel(x, y, color);

        //Comienza a dibujar la línea
        //Cuando la pendiente m=dy/dx es menor a 1
        if(dx > dy) {
            p = 2*dy - dx;
            const1 = 2*dy;
            const2 = 2*(dy-dx);
            while (x != x2)
            {
                x = x + stepX;
                if (p < 0)
                {
                    p = p + const1;
                } else
                {
                    y = y + stepY;
                    p = p + const2;
                }
                setPixel(x, y, color);
            }
        }
        //Cuando la pendiente m=dy/dx es mayor a 1
        else {
            p = 2*dx - dy;
            const1 = 2*dx;
            const2 = 2*(dx-dy);
            while (y != y2){
                y = y + stepY;
                if (p < 0){
                    p = p + const1;
                }
                else {
                    x = x + stepX;
                    p = p + const2;
                }

                setPixel(x, y, color);
            }
        }
    }

    public void boundary_fill(int x, int y, int[] fill_color){
        int[] present_color = new int[4];
        int[] rojo = new int[]{255,0,0,255};
        int[] verde = new int[]{0,255,0,255};
        int[] azul = new int[]{0,0,255,255};
        int[] amarillo =  new int[]{220,220,0,255};

        present_color=raster.getPixel(x,y,new int[4]);

        if (!Arrays.equals(present_color, rojo) && !Arrays.equals(present_color, verde) && !Arrays.equals(present_color, azul) && !Arrays.equals(present_color, amarillo) && !Arrays.equals(present_color, fill_color)) {

            try {
                raster.setPixel(x,y,fill_color);
                boundary_fill(x+1,y,fill_color);
                boundary_fill(x-1,y,fill_color);
                boundary_fill(x,y+1,fill_color);
                boundary_fill(x,y-1,fill_color);
            } catch (Exception e) {

            }
        }

    }

    /*public void boundary_fill(int x, int y, int[] fill_color, int [] boundary_color)
    {
        int[] present_color;
        present_color=buffImage.getRaster().getPixel(x,y, new int [4]);

        if(!(Arrays.equals(present_color,boundary_color)) && !(Arrays.equals(present_color,fill_color))){
            try
            {
                buffImage.getRaster().setPixel(x,y, fill_color);
                boundary_fill((x+1),y,fill_color,boundary_color);
                boundary_fill((x-1),y,fill_color,boundary_color);
                boundary_fill(x,(y+1),fill_color,boundary_color);
                boundary_fill(x,(y-1),fill_color,boundary_color);
            }catch(Exception n)
            {

            }
        }

    }*/

    private void setPixel(int x, int y, int[] color){
        try {
            raster.setPixel(x,y,color);
        } catch (ArrayIndexOutOfBoundsException e) {

        }
    }

    public void trasladar(int Tx, int Ty){

        int cont=1;
        boolean sale=false;
        for(int i=0;i<figT.length;i++){
            for(int j=0;j<figT[i].length;j++){
                //System.out.println(figO[i][j]);

                if(cont%2!=0){
                    figT[i][j]=figO[i][j]+Tx;
                    //System.out.println("x: "+figT[i][j]);
                }
                else{
                    figT[i][j]=figO[i][j]+Ty;
                    //System.out.println("y: "+figT[i][j]);
                }
                cont++;
                if(figT[i][j]>500){
                    sale=true;
                }
            }
        }

        dibujaCuadrado(figT);
        this.repaint();

        if(sale){
            JOptionPane.showMessageDialog(this,"La figura sale del area");
        }
    }

    public void escalar(double Sx, double Sy, int Xf, int Yf){

        int cont=1;
        boolean sale=false;
        for(int i=0;i<figT.length;i++){
            for(int j=0;j<figT[i].length;j++){
                //System.out.println(figO[i][j]);

                //X
                if(cont%2!=0){

                    if (Xf == -1 && Yf == -1) {
                        figT[i][j] = (int) Math.round(figO[i][j] * Sx);
                    } else {
                        figT[i][j] = (int) Math.round(figO[i][j] * Sx + (1 - Sx) * Xf);
                    }


                    //System.out.println("x: "+figT[i][j]);
                }//Y
                else{
                    if (Xf == -1 && Yf == -1) {
                        figT[i][j] = (int) Math.round(figO[i][j] * Sy);
                    } else {
                        figT[i][j] = (int) Math.round(figO[i][j] * Sy + (1 - Sy) * Xf);
                    }
                    //System.out.println("y: "+figT[i][j]);
                }
                cont++;
                if(figT[i][j]>500){
                    sale=true;
                }
            }
        }

        dibujaCuadrado(figT);
        this.repaint();

        if(sale){
            JOptionPane.showMessageDialog(this,"La figura sale del area");
        }
    }

    public void rotar(double theta, int Xr, int Yr){
        int cont=1;
        boolean sale=false;
        for(int i=0;i<figT.length;i++){
            for(int j=0;j<figT[i].length;j++){
                //System.out.println(figO[i][j]);

                //X
                if(cont%2!=0){

                    if (Xr == -1 && Yr == -1) {
                        figT[i][j] = (int) Math.round(figO[i][j]*Math.cos(theta)-figO[i][j+1]*Math.sin(theta));
                        System.out.println("La X="+figO[i][j]+" Y="+figO[i][j+1]+" Nueva X="+figT[i][j]);
                    } else {
                        figT[i][j] = (int) Math.round(  Xr+(figO[i][j]-Xr)*Math.cos(theta)  - (figO[i][j+1]-Yr)*Math.sin(theta)  );
                    }


                    //System.out.println("x: "+figT[i][j]);
                }//Y
                else{
                    if (Xr == -1 && Yr == -1) {
                        figT[i][j] = (int) Math.round(figO[i][j]*Math.cos(theta)+figO[i][j-1]*Math.sin(theta));
                    } else {
                        figT[i][j] = (int) Math.round(  Yr+(figO[i][j]-Yr)*Math.cos(theta)  +  (figO[i][j-1]-Xr)*Math.sin(theta)  );
                    }
                    //System.out.println("y: "+figT[i][j]);
                }
                cont++;
                if(figT[i][j]>500 ||  figT[i][j]<0){
                    sale=true;
                }
            }
        }

        dibujaCuadrado(figT);
        this.repaint();

        if(sale){
            JOptionPane.showMessageDialog(this,"La figura sale del area");
        }
    }


    public static double[][] Multiplicar(double[][] m1,double[][] m2,double[][]multiplicacion)
    {
        for (int x=0; x < multiplicacion.length; x++)
        {
            for (int y=0; y < multiplicacion[x].length; y++)
            {
                for (int z=0; z<m1[0].length; z++)
                {
                    multiplicacion [x][y] += m1[x][z]*m2[z][y];
                }
            }
        }
        return multiplicacion;
    }

}

